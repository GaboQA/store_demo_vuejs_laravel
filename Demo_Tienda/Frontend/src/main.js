// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2'
import LoadScript from 'vue-plugin-load-script'

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css'

require('../node_modules/bootstrap/dist/css/bootstrap.css')

Vue.use(LoadScript)
Vue.config.productionTip = false
Vue.use(VueSweetalert2)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  render: h => h(App)
})
