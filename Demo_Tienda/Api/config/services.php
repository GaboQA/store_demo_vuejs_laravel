<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    /*'facebook' => [
        'client_id' => '2776014579345886',
        'client_secret' => '188a781fa09f76ed0ed48b089dc0473b',
        'redirect' => 'http://localhost:8000/auth/facebook/sociallogin',
    ],*/
    'google' => [ 
        'client_id' => '317165868281-ntucamqonb74f7t0khokgpcq3p6l5taq.apps.googleusercontent.com',
        'client_secret' => 'CyGUmtwmKuSUfuLTO1GQ5fMa',
        'redirect' => 'http://localhost:8000/api/google/callback',
    ],
        
    /*'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => 'http://localhost:8000/api/google/callback',
    ],*/
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => 'http://localhost:8000/api/facebook/callback',
    ],
];
