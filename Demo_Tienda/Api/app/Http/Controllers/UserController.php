<?php

namespace App\Http\Controllers;
//Extenciones de Login with facebook and google
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use Laravel\Socialite\Facades\Socialite;
use Socialite;
//-----------------------------------------------
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
//use JWTAuth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Session;
use Illuminate\Support\Facades\Cache;
use App\Task;

class UserController extends Controller
{   

    public function register(Request $request)
    {   

        $request->verification = false;
        $request->accountant = 1;
        $request->history = null;
        $request->relation_items = null;
        $request->type = "n";

        $request->validate([
            'name' => ['required'],
            'date' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'location' => ['required'],
            'gender' => ['required'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);
        //Create user with the register
        $user = User::create([
            'name' => $request->name,
            'date' => $request->date,
            'image' => $request->image,
            'email' => $request->email,
            'location' => $request->location,
            'gender' => $request->gender,
            'history' => $request->history,
            'relation_items' => $request->relation_items,
            'type' => $request->type,
            'password' => Hash::make($request->password),
            'accountant' => $request->accountant,
            'verification' => $request->verification
        ]);
        $email = $request;
        //session(['emai l' => $Email]);
        Mail::send('emails.welcome',['email'=>$email] , function ($message) use ($email){
            // dd($email->email);
            $message->from('gabrielna1999@gmail.com', 'Boutique Verde Limón');    
            $message->to($email->email,'Boutique Verde Limón')->subject('Verfique su cuenta');
            // Distroy sessions
            // Session::flush();  
        });
        $token = JWTAuth::fromUser($user);
        return response()->json(compact('user','token'), 201);
    }
    public function login(Request $request)
    {
        $credentials = $request->json()->all();
        try {
            if(! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Credenciales invalidas'], 400);
            }
            
            if(JWTAuth::user()->verification != true){
                //session_start();
                //session()->regenerate();
                /*$email = JWTAuth::user();
                
                //session(['email' => $Email]);

                Mail::send('emails.welcome',['email'=>$email] , function ($message) use ($email){
                    // dd($email->email);
                    $message->from('gabrielna1999@gmail.com', 'Boutique Verde Limón');    
                    $message->to($email->email,'Boutique Verde Limón')->subject('Verfique su cuenta');
                    // Distroy sessions
                    // Session::flush();  
                }); */
                return response()->json(['error' => 'Verifica tu email'], 400);
            }
            else if(! $token = JWTAuth::attempt($credentials)){
                return response()->json(JWTAuth::user(), 200);
            } 
        } catch(JWTException $e) {
            return response()->json(['error' => 'No se creo el token'], 500);
        }
         return response()->json(compact('token') );
       
    }

    public function logout()
    {
        Auth::logout();
    }


    public function getAuthenticatedUser()
    {
       
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    //Seccion de login con google y facebook
    public function Login_google_facebook(Request $request){
        
        /*$credentials = $request->json()->all();
        try {
            if(! $token = JWTAuth::attempt($credentials)) {
                return response()->json(JWTAuth::user(), 200);
            }
            else if(! $token = JWTAuth::attempt($credentials)){
                return response()->json(['error' => 'Credenciales invalidas'], 400);
            } 
        } catch(JWTException $e) {
            return response()->json(['error' => 'No se creo el token'], 500);
        }
         return response()->json(compact('token') );*/
        
        
        
        
        
        
        
        
        
        
        
        
        
        $credentials = $request->json()->all();

        $opcion1=array('email' => $request->email,
         'password' => $request->password,
         );
        //dd($opcion1);
        //dd($request);
        try {
            //Verificamos si no existen los datos dentro de la base de datos
            if(! $token = JWTAuth::attempt($opcion1)) {
            $request->verification = true;
            $request->accountant = 1;
            $request->history = null;
            $request->relation_items = null;
            $request->location = null;
            $request->gender = null;
            $request->validate([
        ]);
        //Creamos el usuario registrado por medio del login seleccionado
        $user = User::create([
            'name' => $request->name,
            'date' => $request->date,
            'image' => $request->image,
            'email' => $request->email,
            'location' => $request->location,
            'gender' => $request->gender,
            'history' => $request->history,
            'relation_items' => $request->relation_items,
            'type' => $request->type,
            'password' => Hash::make($request->password),
            'accountant' => $request->accountant,
            'verification' => $request->verification
        ]);
        $token = JWTAuth::fromUser($user);
        //Verificamos nuevamente si se encuentra registrado
            if(! $token = JWTAuth::attempt($credentials)){
                return response()->json(JWTAuth::user(), 200);
            }
            }
        } catch(JWTException $e) {
            return response()->json(['error' => 'La cuenta ingresada ya existe '], 500);
        }
         return response()->json(compact('token') );
    }


    //Editar el perfil del usuario

    public function index()
    {
        
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
        
    }
 
    public function show($id)
    {
        return User::find($id);
    }

    public function update(Request $request, $id)
    {
        $task = User::findOrFail($id);
        $task->update($request->all());

        return $task;
    }

}
