<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('sendemail', function () {
});
Route::get('email', 'VerificateEmailController@email');
//Routes google and facebook
Route::get('api/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('api/{provider}/callback', 'Auth\LoginController@handleProviderCallback');