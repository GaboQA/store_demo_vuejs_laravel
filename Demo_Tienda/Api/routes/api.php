<?php

use Illuminate\Http\Request;

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('profile', 'UserController@getAuthenticatedUser');
Route::post('/logout', 'UserController@logout');
Route::get('/email', 'VerificateEmailController@email');
//Editar profile
Route::get('users', 'UserController@index');
Route::get('users/{id}', 'UserController@show');
Route::put('users/{id}', 'UserController@update');
//Routes google and facebook
//Route::get('/api/{provider}', 'Auth\LoginController@redirectToProvider');
//Route::get('/api/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
//Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
//Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');
Route::post('/social', 'UserController@Login_google_facebook');


Route::get('tasks', 'TaskController@index');
Route::get('tasks/{id}', 'TaskController@show');
Route::post('tasks', 'TaskController@store');
Route::put('tasks/{id}', 'TaskController@update');
Route::delete('tasks/{id}', 'TaskController@delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




